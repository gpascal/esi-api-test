import { assert } from 'chai';
import * as _ from 'lodash';
// import * as fs from 'fs';

import { Application } from '../../lib/Application';
import { EsiWalletClient } from '../../lib/esi';

describe('EsiWalletClient', function () {
    this.timeout(60000);
    const app = new Application();
    let client: EsiWalletClient;
    let characterId;

    before(async () => {
        await app.initialize();
        client = app.dependencies.esiClients.esiWalletClient;
        characterId = app.dependencies.common.config.test.ids.calaen;
    });

    after(async () => {
        await app.shutdown();
    });

    it('getCharacterTransactions', async () => {
        const transactions = await client.getCharacterTransactions(characterId);
        assert.isArray(transactions);
        // fs.writeFileSync('./transactions.json', JSON.stringify(transactions));
    });

    it('getCharacterTransactionsFromPreviousTransaction', async () => {
        try {
            const transactions = await client.getCharacterTransactions(characterId);
            console.log(`${transactions.length} initial transactions`);
            console.log(`Oldest = ${JSON.stringify(_.minBy(transactions, t => new Date(t.date)), null, 2)}`);
            assert.isArray(transactions);
            const sample = _.sample(transactions);
            console.log(`Sample ${sample.transaction_id} date is ${sample.date}`);
            const transactionsFrom = await client.getCharacterTransactions(characterId, sample.transaction_id);
            console.log(`${transactionsFrom.length} transactions from`);
            _.each(transactionsFrom, t => assert.isAtMost(new Date(t.date).getTime(), new Date(sample.date).getTime()));
        } catch (e) {
            console.error(e);
        }
    });
});
