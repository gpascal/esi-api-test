import { assert } from 'chai';
import * as bluebird from 'bluebird';

import { IEsiRegion } from '../../lib/esi/types';
import { Application } from '../../lib/Application';
import { EsiUniverseClient } from '../../lib/esi';

describe('EsiUniverseClient', function () {
    this.timeout(60000);
    const app = new Application();
    let client: EsiUniverseClient;

    before(async () => {
        await app.initialize();
        client = app.dependencies.esiClients.esiUniverseClient;
    });

    after(async () => {
        await app.shutdown();
    });

    it('getCategories', async () => {
        const categories = await client.getCategories();
        assert.isArray(categories);
    });

    it('getRegions', async () => {
        const regionIds = await client.getRegions();
        console.log(`Got ${regionIds.length} region ids`);
        const regions = await bluebird.map(regionIds, regionId => client.getRegion(regionId), { concurrency: 5 });
        console.log(`Regions: ${regions
            .map(regionToString)
            .join('\n')}`);
    });

    it('getGroups', async () => {
        const groups = await client.getGroups();
        console.log(`Got ${groups.length} groups`);
    });

    function regionToString(region: IEsiRegion): string {
        return `${region.name}=${region.region_id}`;
    }
});
