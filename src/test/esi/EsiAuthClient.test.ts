import { assert } from 'chai';

import { Application } from '../../lib/Application';
import { ALL_SCOPES, EsiAuthClient } from '../../lib/esi';
import { IConfig } from '../../lib/config';

describe('EsiAuthClient', function () {
    this.timeout(60000);

    const app = new Application();
    let config: IConfig;
    let client: EsiAuthClient;

    before(async () => {
        await app.initialize();
        config = app.dependencies.common.config;
        client = app.dependencies.esiClients.esiAuthClient;
    });

    after(async () => {
        await app.shutdown();
    });

    it.skip('Authenticates a new character', async () => {
        const token = await client.authenticateNewCharacter(ALL_SCOPES);
        assert.isNumber(token.id);
        assert.isNotEmpty(token.access);
        assert.isNotEmpty(token.refresh);
        assert.isNumber(token.expires);
        assert.isAtLeast(token.expires, Date.now());
        assert.isArray(token.scopes);
    });

    it('Get access token', async () => {
        const accessToken = await client.getAccessToken(config.test.ids.eloKhamez, ALL_SCOPES);
        assert.isString(accessToken);
        assert.isNotEmpty(accessToken);
    });
});
