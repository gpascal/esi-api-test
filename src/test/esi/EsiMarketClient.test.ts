import { assert } from 'chai';

import { EsiMarketOrderType } from '../../lib/esi/types';
import { Application } from '../../lib/Application';
import { EsiMarketClient } from '../../lib/esi';
import { Region } from '../../lib/util/Locations';

describe('EsiMarketClient', function () {
    this.timeout(60000);
    const app = new Application();
    let client: EsiMarketClient;

    before(async () => {
        await app.initialize();
        client = app.dependencies.esiClients.esiMarketClient;
    });

    after(async () => {
        await app.shutdown();
    });

    it('getRegionOrders', async () => {
        const marketOrders = await client.getMarketOrders(Region.DOMAIN, null, EsiMarketOrderType.ALL);
        assert.isArray(marketOrders);
        console.log(`market=${marketOrders.length}`);
        console.log(marketOrders.map(o => JSON.stringify(o)).join('\n'));
    });

    it('getRegionOrders', async () => {
        const marketOrders = await client.getCharacterOrders(app.dependencies.common.config.test.ids.eloKhamez);
        console.log(JSON.stringify(marketOrders));
    });
});
