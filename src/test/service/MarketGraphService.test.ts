import { Application } from '../../lib/Application';
import { config, IConfig } from '../../lib/config';
import { LiveGraphService } from '../../lib/service/LiveGraphService';

describe.only('MarketGraphService', async function () {
    this.timeout(0);

    const app = new Application();
    let service: LiveGraphService;

    before(async () => {
        config.http.headless = true;
        await app.initialize(config);
        service = app.dependencies.services.liveGraphService;
    });

    after(async () => {
        await app.shutdown();
    });

    it('get current Amarr trades WatchReports', async () => {
        await service.startGrapher(config.test.ids.eloKhamez);
        return new Promise(() => null);
    });
});
