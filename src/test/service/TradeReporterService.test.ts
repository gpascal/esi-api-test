import { Application } from '../../lib/Application';
import { IConfig } from '../../lib/config';
import { TradeReporterService } from '../../lib/service';
import { Region } from '../../lib/util/Locations';

describe('TradeReporterService', async function () {
    this.timeout(0);

    const app = new Application();
    let config: IConfig;
    let service: TradeReporterService;

    before(async () => {
        await app.initialize();
        service = app.dependencies.services.tradeReporterService;
        config = app.dependencies.common.config;
    });

    after(async () => {
        await app.shutdown();
    });

    it('get current Amarr trades WatchReports', async () => {
        const reports = await service.buildTradeReport(config.test.ids.eloKhamez, Region.THE_FORGE, Region.DOMAIN);
        await app.dependencies.dao.tradeReportDao.insert(reports);
    });
});
