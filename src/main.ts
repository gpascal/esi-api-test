import 'source-map-support/register';
import { Application } from './lib/Application';

const APP_NAME = 'esi-api-test';
const logger = require('./lib/util/Logger')(module);

async function main() {
    console.log('starting');
    logger.info(`##### STARTING ${APP_NAME} on ${new Date().toUTCString()} #####`);
    const application = new Application();
    await application.initialize();

    async function exit() {
        await application.shutdown();
        logger.info(`##### TERMINATING ${APP_NAME} on ${new Date().toUTCString()} #####`);
        process.exit(0);
    }

    process.on('SIGTERM', exit);
    process.on('uncaughtException', (err) => {
        logger.error(`Uncaught error: ${err.toString()}`);
    });
}

// noinspection JSIgnoredPromiseFromCall
main();
