import { Router } from 'express';
import * as _ from 'lodash';

import { IRouter } from './HttpServer';
import { IConfig } from '../config';
import { EventHub, EventName } from '../util';
import { TradeWatcherService } from '../service';

export class TradesWatcherRouter implements IRouter {
    public readonly router: Router;
    private config: IConfig;
    private eventHub: EventHub;
    private tradeWatcherService: TradeWatcherService;

    constructor(config: IConfig, eventHub: EventHub, tradeWatcherService: TradeWatcherService) {
        this.config = config;
        this.eventHub = eventHub;
        this.tradeWatcherService = tradeWatcherService;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();

        router.post('/characters/:characterId/trades-watcher/:stationId', async (req, res) => {
            const { characterId, stationId } = req.params;
            if (_.isEmpty(stationId) || _.isEmpty(characterId)) return res.sendStatus(400);

            this.tradeWatcherService.startTradeWatcher(parseInt(characterId), parseInt(stationId))
                .catch(err => this.eventHub.publish(EventName.TRADE_WATCHER_ERROR,
                    { name: err.name, message: err.message, stack: err.stack }));

            res.sendStatus(202);
        });
        router.post('/characters/:characterId/trades-watcher/:stationId/heartbeat', async (req, res, next) => {
            try {
                const { characterId, stationId } = req.params;
                this.tradeWatcherService.bumpWatcher(parseInt(characterId), parseInt(stationId));
                res.status(204);
            } catch (e) {
                next(e);
            }
        });

        return router;
    }
}
