import { Router } from 'express';
import * as _ from 'lodash';

import { IRouter } from './HttpServer';
import { IConfig } from '../config';
import { EventHub, EventName, IMultiStageProgressAware } from '../util';
import { TradeOpportunitiesDao } from '../data';
import { ITradeOpportunitiesOptions, TradeOpportunitiesService } from '../service';

const logger = require('../util/Logger')(module);

export class TradeOpportunitiesRouter implements IRouter {
    public readonly router: Router;
    private config: IConfig;
    private eventHub: EventHub;
    private tradeOpportunitiesDao: TradeOpportunitiesDao;
    private tradeOpportunitiesService: TradeOpportunitiesService;

    constructor(config: IConfig, eventHub: EventHub, tradeOpportunitiesService: TradeOpportunitiesService,
                tradeOpportunitiesDao: TradeOpportunitiesDao) {
        this.config = config;
        this.eventHub = eventHub;
        this.tradeOpportunitiesService = tradeOpportunitiesService;
        this.tradeOpportunitiesDao = tradeOpportunitiesDao;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();
        router.get('/trade-opportunities', async (req, res) => {
            const reports = await this.tradeOpportunitiesDao.getAll();
            res.json(reports);
        });

        router.post('/trade-opportunities', async (req, res) => {
            const { state } = req.query;
            const { srcRegionId, dstRegionId, options: rawOptions } = req.body;
            const options = this.parseOptions(rawOptions);
            logger.info(`Compute opportunities ${srcRegionId}(${typeof srcRegionId}) ${dstRegionId} ${state}`);
            if (!_.isNumber(srcRegionId) || !_.isNumber(dstRegionId)) return res.sendStatus(400);

            this.tradeOpportunitiesService.computeTradeOpportunities(srcRegionId, dstRegionId, options, this.feedbackFollower(state))
                .then(report => this.eventHub.publish(EventName.TRADE_OPPORTUNITIES_COMPUTED, { ...report, state }))
                .catch(err => this.eventHub.publish(EventName.TRADE_OPPORTUNITIES_ERROR, err));

            res.sendStatus(202);
        });

        router.delete('/trade-opportunities/:opportunityId', async (req, res) => {
            const opportunityId = decodeURIComponent(req.params.opportunityId);
            await this.tradeOpportunitiesDao.delete(opportunityId);
            res.sendStatus(204);
        });
        return router;
    }

    private feedbackFollower(state: string): IMultiStageProgressAware {
        return {
            progress: _.throttle(
                stages => this.eventHub.publish(EventName.TRADE_OPPORTUNITIES_PROGRESS, { state, stages }),
                1000,
                { leading: true, trailing: true }),
        };
    }

    private parseOptions(rawOptions): ITradeOpportunitiesOptions {
        return {
            minBuyPrice: parseInt(rawOptions.minBuyPrice),
            maxBuyPrice: parseInt(rawOptions.maxBuyPrice),
            minTradedVolumePerDay: parseInt(rawOptions.minTradedVolumePerDay),
            minTradesPerDay: parseInt(rawOptions.minTradesPerDay),
        };
    }
}
