import * as express from 'express';
import { Express, NextFunction, Request, Response, Router } from 'express'; // tslint:disable-line:no-duplicate-imports
import { Server } from 'http';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as _ from 'lodash';
import * as querystring from 'querystring';

import { ICommonDeps, ILifeCycleAware } from '../util';
import { IConfig } from '../config';

const logger = require('../util/Logger')(module);

export interface IRouter {
    router: Router;
}

export class HttpServer implements ILifeCycleAware {
    private config: IConfig;
    private app: Express;
    private server: Server;
    private routers: IRouter[];

    constructor(commonDeps: ICommonDeps, routers: IRouter[]) {
        this.routers = routers;
        this.config = commonDeps.config;
        this.app = express();
    }

    public async initialize(): Promise<void> {
        const { headless, port } = this.config.http;

        if (headless) {
            // No HTTP server in headless mode
            return;
        }

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded());
        this.app.use(cors());
        this.app.use(this.logRequest);
        this.app.use('/api', this.buildApiRouter());
        this.app.use((req, res) => res.status(404).send(`Not Found (${req.path})`));
        this.app.use(this.handleError);

        this.server = this.app.listen(port);
        logger.info(`Http server started listening on :${port}`);
    }

    public async shutdown(): Promise<void> {
        if (this.config.http.headless) {
            return;
        }

        return new Promise<void>(resolve => this.server.close(resolve));
    }

    private buildApiRouter(): Router {
        const router = Router();

        _.each(this.routers, apiRouter => router.use(apiRouter.router));

        return router;
    }

    private handleError = async (error, req: Request, res: Response, next: NextFunction): Promise<void> => {
        logger.error(`Http ErrorHandler caught ${error.message} ${error.stack}`);
        res.status(500).send(error);
    }

    private logRequest = (req: Request, res, next) => {
        logger.silly(`Incoming ${req.method} ${req.path}?${querystring.stringify(req.query || {})}`);
        next();
    }
}
