import { Router } from 'express';

import { IRouter } from './HttpServer';
import { GraphDao } from '../data';
import { GraphService } from '../service';

export class GraphRouter implements IRouter {
    public readonly router: Router;

    private graphDao: GraphDao;
    private graphService: GraphService;

    constructor(graphDao: GraphDao, graphService: GraphService) {
        this.graphDao = graphDao;
        this.graphService = graphService;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();

        router.get('/characters/:characterId/graphs/transactions', async (req, res, next) => {
            try {
                const { characterId } = req.params;
                const graphPoints = await this.graphService.buildTransactionGraphData(parseInt(characterId));
                res.json(graphPoints);
            } catch (err) {
                next(err);
            }
        });

        router.get('/characters/:characterId/graphs/worth', async (req, res, next) => {
            try {
                const { characterId } = req.params;
                const graphPoints = await this.graphDao.getByCharacter(parseInt(characterId));
                res.json(graphPoints);
            } catch (err) {
                next(err);
            }
        });

        return router;
    }
}
