import { Router } from 'express';
import * as _ from 'lodash';

import { IRouter } from './HttpServer';
import { IConfig } from '../config';
import { TradeReporterService } from '../service';
import { TradeReportDao } from '../data';
import { EventHub, EventName, IMultiStageProgressAware } from '../util';

const logger = require('../util/Logger')(module);

export class TradeReportsRouter implements IRouter {
    public readonly router: Router;
    private config: IConfig;
    private eventHub: EventHub;
    private tradeReporterService: TradeReporterService;
    private tradeReportDao: TradeReportDao;

    constructor(config: IConfig, eventHub: EventHub, tradeReporterService: TradeReporterService, tradeCampaignDao: TradeReportDao) {
        this.config = config;
        this.eventHub = eventHub;
        this.tradeReporterService = tradeReporterService;
        this.tradeReportDao = tradeCampaignDao;
        this.router = this.buildRouter();
    }

    private buildRouter(): Router {
        const router = Router();

        router.get('/characters/:characterId/trade-reports', async (req, res) => {
            const { characterId } = req.params;
            if (_.isEmpty(characterId)) return res.sendStatus(400);
            const reports = await this.tradeReportDao.getByCharacter(parseInt(characterId));
            res.json(reports);
        });

        router.post('/characters/:characterId/trade-reports', async (req, res) => {
            const { characterId } = req.params;
            const { state } = req.query;
            const { srcRegionId, dstRegionId } = req.body;
            if (!_.isNumber(srcRegionId) || !_.isNumber(dstRegionId) || _.isEmpty(characterId)) return res.sendStatus(400);

            this.tradeReporterService.buildTradeReport(parseInt(characterId), srcRegionId, dstRegionId, this.feedbackFollower(state))
                .then(report => this.eventHub.publish(EventName.TRADE_REPORTS_COMPUTED, { ...report, state }))
                .catch(err => this.eventHub.publish(EventName.TRADE_REPORTS_ERROR,
                    { name: err.name, message: err.message, stack: err.stack }));

            res.sendStatus(202);
        });

        router.delete('/characters/:characterId/trade-reports/:reportId', async (req, res) => {
            const reportId = decodeURIComponent(req.params.reportId);
            await this.tradeReportDao.delete(reportId);
            res.sendStatus(204);
        });

        return router;
    }

    private feedbackFollower(state: string): IMultiStageProgressAware {
        return {
            progress: _.throttle(
                stages => console.log(`Publishing report progress ${JSON.stringify(stages)}`) as any ||
                    this.eventHub.publish(EventName.TRADE_REPORTS_PROGRESS, { state, stages }),
                1000,
                { leading: true, trailing: true }),
        };
    }
}
