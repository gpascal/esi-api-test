import { ICommonDeps } from '../util';
import { AbstractDao, IId } from './AbstractDao';

export enum CacheType {
    CATEGORIES = 'categories',
    CATEGORY = 'category',
    REGION = 'region',
    TYPE = 'type',
    GROUP = 'group',
    GROUPS = 'groups',
    REGIONS = 'regions',
    MARKET_HISTORY = 'market_history',
    MARKET_ORDERS = 'market_orders',
    TRADE_OPPORTUNITY = 'trade_opportunity',
    MARKET_TYPES = 'market_types',
    MARKET_GROUPS = 'market_groups',
    MARKET_GROUP_INFO = 'market_group_info',
    WALLET_TRANSACTION = 'wallet_transaction',
    STATION = 'station',
    SYSTEM = 'system',
    CONSTELLATION = 'constellation',
    CONSTELLATIONS = 'constellations',
    CHARACTER_ORDERS_HISTORY = 'character_orders_history',
    SYSTEMS = 'systems',
    CHARACTER_ASSETS = 'character_assets',
    MARKET_PRICE = 'market_price',
    WALLET_JOURNAL = 'wallet_journal',
}

export interface ICacheEntry<T> {
    id: string;
    type: CacheType;
    etag: string;
    data: T;
}

export class CacheDao extends AbstractDao<ICacheEntry<any>> {
    private static readonly COLLECTION = 'cache';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, CacheDao.COLLECTION);
    }

    public async get<T>(id: IId): Promise<ICacheEntry<T>> {

        return super.get(id);
    }

    public async update<T>(id: IId, model: ICacheEntry<T>, options): Promise<ICacheEntry<T>> {
        return super.update(id, model, options);
    }

    public async insert<T>(model: ICacheEntry<T>): Promise<ICacheEntry<T>> {
        return super.insert(model);
    }
}
