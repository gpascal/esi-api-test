import { AbstractDao } from './AbstractDao';
import { ICommonDeps } from '../util';
import { EntityId } from '../esi';

export interface IRegion {
    id: EntityId;
    name: string;
    description?: string;
}

export class RegionDao extends AbstractDao<IRegion> {
    private static readonly COLLECTION = 'regions';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, RegionDao.COLLECTION);
    }
}
