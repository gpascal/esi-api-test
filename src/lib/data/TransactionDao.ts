import { AbstractDao } from './AbstractDao';
import { ICommonDeps } from '../util';
import { EntityId, IEsiTransaction } from '../esi';

export interface ITransaction extends IEsiTransaction {
    id: EntityId;
    characterId: EntityId;
}

export class TransactionDao extends AbstractDao<ITransaction> {
    private static readonly COLLECTION = 'regions';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, TransactionDao.COLLECTION);
    }
}
