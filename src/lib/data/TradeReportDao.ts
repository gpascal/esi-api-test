import { AbstractDao } from './AbstractDao';
import { ICommonDeps } from '../util';
import { DateString, EntityId, IEsiType } from '../esi';

export interface ITradeReports {
    id: string;
    date: DateString;
    characterId: EntityId;
    srcRegionId: EntityId;
    srcRegionName: string;
    dstRegionId: EntityId;
    dstRegionName: string;
    reports: ITradeReport[];
}

export interface ITradeReport {
    id: string;
    type: IEsiType;
    campaignStart: Date;
    campaignEnd: Date;
    boughtItems: number;
    averageBuyPrice: number;
    soldItems: number;
    averageSellPrice: number;
    remainingItems: number;
    estimatedCompletionDate: Date;
    profit: number;
    profitPerItem: number;
    processedTransactions: EntityId[];
}

export class TradeReportDao extends AbstractDao<ITradeReports> {
    private static readonly COLLECTION = 'trade-reports';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, TradeReportDao.COLLECTION);
    }

    public async findByRegions(srcRegionId: EntityId, dstRegionId: EntityId): Promise<ITradeReports|null> {
        const reports = await this.getCollection().find({ srcRegionId, dstRegionId }).sort({ date: -1 }).limit(1).toArray();
        return reports[0] || null;
    }

    public getByCharacter(characterId: EntityId) {
        return this.getCollection().find({ characterId }).toArray();
    }
}
