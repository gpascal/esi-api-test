import { AbstractDao } from './AbstractDao';
import { ICommonDeps } from '../util';
import { IToken } from '../esi';

export interface IToken {
    id: number; // character_id
    access: string;
    refresh: string;
    expires: number;
    scopes: string[];
}

export class AuthDao extends AbstractDao<IToken> {
    private static readonly COLLECTION = 'character-auth';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, AuthDao.COLLECTION);
    }
}
