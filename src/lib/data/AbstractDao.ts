import { Collection, MongoClient, Db, MongoError } from 'mongodb';
import * as bluebird from 'bluebird';

import { IConfig } from '../config';
import { IObservable, ObservableUtils, ICommonDeps, ILifeCycleAware } from '../util';

export type IId = number | string;

interface IIdBearer {
    id: IId;
}

export enum DaoEvent {
    change = 'change',
}

// tslint:disable-next-line:variable-name
const _idProjection = { _id: 0 };

export abstract class AbstractDao<TModel extends IIdBearer> implements ILifeCycleAware, IObservable<DaoEvent> {
    protected indexes: (string|string[])[] = [];
    protected config: IConfig;
    private client: MongoClient;
    private collectionName: string;
    private observable: ObservableUtils<DaoEvent>;

    protected constructor(commonDeps: ICommonDeps, collectionName: string) {
        this.collectionName = collectionName;
        this.config = commonDeps.config;
        const { host, port } = this.config.mongo;
        this.client = new MongoClient(`mongodb://${host}:${port}`, { useNewUrlParser: true, poolSize: 15 });
        this.observable = new ObservableUtils();
    }

    public async initialize(): Promise<void> {
        await this.client.connect();
        await this.getDb().createCollection<TModel>(this.collectionName);

        const indexAlreadyExists = await this.getCollection().indexExists('id');
        if (!indexAlreadyExists) {
            await this.getCollection().createIndex('id', { unique: true });
        }

        await bluebird.each(this.indexes, async (index) => {
            const indexAlreadyExists = await this.getCollection().indexExists(index);
            if (!indexAlreadyExists) {
                await this.getCollection().createIndex(index);
            }
        });
    }

    public async shutdown(): Promise<void> {
        await this.client.close();
    }

    public on(eventName: DaoEvent, callback) {
        this.observable.register<DaoEvent>(eventName, callback);
    }

    public async getAll(): Promise<TModel[]> {
        return this.getCollection().find({}, { projection: _idProjection }).toArray();
    }

    public async get(id: IId): Promise<TModel> {
        const result = await this.getCollection().findOne({ id }, { projection: _idProjection });
        if (result === null) {
            throw new Error(`can not find id ${id} in collection ${this.collectionName}`);
        }
        return result;
    }

    public async getMany(ids: IId[]): Promise<TModel[]> {
        return this.getCollection().find({ id: { $in: ids } }).toArray();
    }

    public async insert(model: TModel): Promise<TModel> {
        await this.getCollection().insertOne(model);
        this.observable.trigger(DaoEvent.change, {});
        return this.get(model.id);
    }

    public async insertMany(models: TModel[]): Promise<TModel[]> {
        await this.getCollection().insertMany(models);
        this.observable.trigger(DaoEvent.change);
        return this.getMany(models.map(m => m.id));
    }

    public async update(id: IId, model: TModel, { upsert = false } = {}): Promise<TModel> {
        const collection = this.getCollection();
        const options = { upsert, returnOriginal: false, projection: _idProjection };
        try {
            const updateRes = await collection.findOneAndUpdate({ id }, { $set: model }, options);
            this.observable.trigger(DaoEvent.change);
            return updateRes.value;
        } catch (e) {
            if (upsert && e instanceof MongoError && e.code === 11000) {
                // Multiple upserts resulted in a duplicate_key_error => retry with upsert=false
                upsert && console.log(`Upsert ${id} => 1000 duplicate key error => retry without upsert`);
                return this.update(id, model);
            }
            throw e;
        }
    }

    public async delete(id: IId): Promise<void> {
        await this.getCollection().deleteOne({ id });
        this.observable.trigger(DaoEvent.change);
    }

    protected getCollection(): Collection<TModel> {
        const db = this.getDb();
        return db.collection<TModel>(this.collectionName);
    }

    protected getDb(): Db {
        return this.client.db(this.config.mongo.db);
    }
}
