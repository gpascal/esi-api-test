import { ICommonDeps } from '../util';
import { AbstractDao } from './AbstractDao';
import { IGraphPoint } from './types';
import { EntityId } from '../esi';

export class GraphDao extends AbstractDao<IGraphPoint> {
    private static readonly COLLECTION = 'graph';

    constructor(commonDeps: ICommonDeps) {
        super(commonDeps, GraphDao.COLLECTION);
    }

    public async getByCharacter(characterId: EntityId): Promise<IGraphPoint[]> {
        return this.getCollection()
            .find({ characterId })
            .sort({ date: 1 })
            .toArray();
    }
}
