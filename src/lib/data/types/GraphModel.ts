import { EntityId } from '../../esi/types';

export enum GraphType {
    WORTH = 'worth',
}

export interface IGraphPoint {
    id: string;
    date: string;
    characterId: EntityId;
    graphType: GraphType;
}

export interface IWorthGraphPoint extends IGraphPoint {
    walletWorth: number;
    sellOrdersWorth: number;
    assetsWorth: number;
    totalWorth: number;
}
