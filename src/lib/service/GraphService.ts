import * as _ from 'lodash';
import * as bluebird from 'bluebird';
import * as moment from 'moment';

import { EntityId, EsiMarketClient, EsiWalletClient, EsiUniverseClient, EsiAssetsClient } from '../esi';
import { GraphDao } from '../data';

export interface ITransactionGraphPoint {
    date: string;
    totalBuy: number;
    totalSell: number;
    balance: number;
}

export class GraphService {
    private graphDao: GraphDao;
    private esiWalletClient: EsiWalletClient;
    private esiMarketClient: EsiMarketClient;
    private esiAssetsClient: EsiAssetsClient;
    private esiUniverseClient: EsiUniverseClient;

    constructor(graphDao: GraphDao, esiWalletClient: EsiWalletClient, esiMarketClient: EsiMarketClient, esiAssetsClient: EsiAssetsClient,
                esiUniverseClient: EsiUniverseClient) {
        this.graphDao = graphDao;
        this.esiWalletClient = esiWalletClient;
        this.esiMarketClient = esiMarketClient;
        this.esiAssetsClient = esiAssetsClient;
        this.esiUniverseClient = esiUniverseClient;
    }

    public async buildTransactionGraphData(characterId: EntityId): Promise<ITransactionGraphPoint[]> {
        const transactions = await this.esiWalletClient.getCharacterTransactions(characterId);
        const journal = _.reduce(await this.esiWalletClient.getCharacterJournal(characterId), (agg, entry) => {
            agg[entry.id] = entry;
            return agg;
        }, {});
        const grouped = _.groupBy(transactions, transaction => this.getDateGroup(transaction.date));

        return bluebird.map(_.keys(grouped), async (date: string) => {
            const groupTransactions = grouped[date];
            const [ buyTrx, sellTrx ] = _.partition(groupTransactions, o => o.is_buy);
            const latestTrx = _.maxBy(groupTransactions, t => new Date(t.date));
            const journalEntry = latestTrx && journal[latestTrx.journal_ref_id];

            return {
                date,
                totalBuy: _.sumBy(buyTrx, t => t.unit_price * t.quantity),
                totalSell: _.sumBy(sellTrx, t => t.unit_price * t.quantity),
                balance: journalEntry && journalEntry.balance,
            };
        });
    }

    private getDateGroup(date) {
        return moment(date)
            .set('ms', 0)
            .set('seconds', 0)
            .set('minutes', 0)
            .toISOString();
    }
}
