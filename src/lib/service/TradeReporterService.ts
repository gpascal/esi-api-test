import * as _ from 'lodash';
import * as bluebird from 'bluebird';
import { v4 as uuid } from 'uuid';

import {
    EntityId,
    EsiMarketClient,
    EsiUniverseClient,
    EsiWalletClient,
    IEsiCharacterMarketOrderOpen,
    IEsiTransaction,
} from '../esi';
import { DaoEvent, ITradeReport, ITradeReports, TradeReportDao } from '../data';
import { EventHub, EventName, IMultiStageProgressAware, IProgressStage, voidProgress } from '../util';

const logger = require('../util/Logger')(module);

interface ITransactionAggregation {
    typeId: EntityId;
    transactions: ITransactionWithRegion[];
}

interface ITransactionWithRegion extends IEsiTransaction {
    region_id: EntityId;
}

export class TradeReporterService {
    private tradeReportDao: TradeReportDao;
    private esiMarketClient: EsiMarketClient;
    private esiUniverseClient: EsiUniverseClient;
    private esiWalletClient: EsiWalletClient;

    constructor(tradeReportDao: TradeReportDao, esiMarketClient: EsiMarketClient, esiUniverseClient: EsiUniverseClient,
                esiWalletClient: EsiWalletClient, eventHub: EventHub) {
        this.tradeReportDao = tradeReportDao;
        this.esiMarketClient = esiMarketClient;
        this.esiUniverseClient = esiUniverseClient;
        this.esiWalletClient = esiWalletClient;

        tradeReportDao.on(DaoEvent.change, (data) => {
            eventHub.publish(EventName.TRADE_REPORTS_UPDATED, { data });
        });
    }

    public async buildTradeReport(characterId: EntityId,
                                  srcRegionId: EntityId,
                                  dstRegionId: EntityId,
                                  progressFollower: IMultiStageProgressAware = voidProgress): Promise<ITradeReports> {
        const progressState: IProgressStage[] = [
            { stage: 0, progress: 0, label: 'Initializing', total: 1, current: 0 },
            { stage: 1, progress: 0, label: 'Locating transactions', total: 1, current: 0 },
            { stage: 2, progress: 0, label: 'Building reports', total: 1, current: 0 },
        ];
        progressFollower.progress(progressState);
        const [ initializingProgress, transactionLocationProgress, buildingReportsProgress ] = progressState;

        const srcRegion = await this.esiUniverseClient.getRegion(srcRegionId);
        const dstRegion = await this.esiUniverseClient.getRegion(dstRegionId);

        const getCharacterTransactionsPromise = this.esiWalletClient.getCharacterTransactions(characterId);
        const getCharacterOrdersPromise = this.esiMarketClient.getCharacterOrders(characterId);
        const [ characterTransactions, characterOrders ] = await Promise.all([
            getCharacterTransactionsPromise,
            getCharacterOrdersPromise,
        ]);
        const allTypeIds = _.uniq(_.compact(_.map(characterTransactions, t => t.type_id)));

        _.assign(initializingProgress, { current: 1, progress: 1 });
        _.assign(transactionLocationProgress, { total: characterTransactions.length });
        progressFollower.progress(progressState);
        let transactionsProcessed = 0;
        const transactionsWithRegion = await bluebird.map(characterTransactions, (t) => {
            const transaction = this.addTransactionRegion(characterId, t);
            transactionsProcessed += 1;
            _.assign(transactionLocationProgress, {
                current: transactionsProcessed,
                progress: transactionsProcessed / characterTransactions.length,
            });
            progressFollower.progress(progressState);
            return transaction;
        }, { concurrency: 5 });

        const transactions: ITransactionAggregation[]  = _.map(allTypeIds, typeId => ({
            typeId,
            transactions: transactionsWithRegion.filter(t => t.type_id === typeId),
        }));

        _.assign(buildingReportsProgress, { total: allTypeIds.length });
        progressFollower.progress(progressState);

        const reports = _.compact(await bluebird.map(allTypeIds, async (typeId): Promise<ITradeReport> => {
            try {
                const typeTransactions = _<ITransactionAggregation[]>(transactions)
                    .find(t => t.typeId === typeId)
                    .transactions;
                const type = await this.esiUniverseClient.getType(typeId);
                const buyTrx = _.filter(typeTransactions, o => o.is_buy && o.region_id === srcRegionId);
                const sellTrx = _.filter(typeTransactions, o => !o.is_buy && o.region_id === dstRegionId);
                const sellOrdersInProgress = _.filter(characterOrders, (o: IEsiCharacterMarketOrderOpen) => {
                    return !o.is_buy_order && o.region_id === dstRegionId && o.type_id === typeId;
                });

                if (buyTrx.length === 0 || sellTrx.length + sellOrdersInProgress.length === 0) return null;

                const earliestBuyOrder = _.minBy(buyTrx, o => new Date(o.date));
                const latestSellOrder = _.maxBy(sellTrx, o => new Date(o.date));
                const boughtItems = _.sumBy(buyTrx, o => o.quantity);
                const soldItems = _.sumBy(sellTrx, o => o.quantity);
                const remainingItems = _.sumBy(sellOrdersInProgress, o => o.volume_remain);

                const campaignStart = new Date(earliestBuyOrder.date);
                const campaignEnd = new Date(latestSellOrder.date);
                const averageTimePerItem = soldItems ? (campaignEnd.getTime() - campaignStart.getTime()) / soldItems : NaN;
                const totalBuyPrice = _.sumBy(buyTrx, t => t.unit_price * t.quantity);
                const totalSellPrice = _.sumBy(sellTrx, t => t.unit_price * t.quantity);
                const profit = totalSellPrice - totalBuyPrice;

                return {
                    id: `${typeId}:${srcRegionId}=>${dstRegionId}{${Date.now()}}-${uuid()}`,
                    type,
                    campaignStart,
                    campaignEnd,
                    boughtItems,
                    soldItems,
                    remainingItems,
                    averageBuyPrice: _.sumBy(buyTrx, o => o.unit_price * o.quantity) / _.sumBy(buyTrx, o => o.quantity),
                    averageSellPrice: _.sumBy(sellTrx, o => o.unit_price * o.quantity) / _.sumBy(sellTrx, o => o.quantity),
                    estimatedCompletionDate: new Date(Date.now() + remainingItems * averageTimePerItem),
                    processedTransactions: _.concat(_.map(buyTrx, o => o.transaction_id), _.map(sellTrx, o => o.transaction_id)),
                    profit,
                    profitPerItem: soldItems ? profit / soldItems : NaN,
                };
            } catch (e) {
                // logger.debug(`Failed to process transaction ${JSON.stringify(transactions)} : ${e.message}`);
                return null;
            } finally {
                _.assign(buildingReportsProgress, {
                    current: buildingReportsProgress.current + 1,
                    total: allTypeIds.length,
                    progress: (buildingReportsProgress.current + 1) / allTypeIds.length,
                });
                progressFollower.progress(progressState);
            }
        }, { concurrency: 5 }));

        const report = {
            id: `${srcRegionId}=>${dstRegionId}-${Date.now()}`,
            date: new Date().toString(),
            characterId,
            srcRegionId,
            srcRegionName: srcRegion.name,
            dstRegionId,
            dstRegionName: dstRegion.name,
            reports,
        };

        await this.tradeReportDao.insert(report);
        return report;
    }

    private async addTransactionRegion(characterId: EntityId, transaction: IEsiTransaction): Promise<ITransactionWithRegion> {
        const isStructure = transaction.location_id > 1000000000;
        let systemId;
        if (isStructure) {
            const structure = await this.esiUniverseClient.getStructure(characterId, transaction.location_id);
            systemId = structure.solar_system_id;
        } else {
            const tStation = await this.esiUniverseClient.getStation(transaction.location_id);
            systemId = tStation.system_id;
        }
        const tSystem = await this.esiUniverseClient.getSystem(systemId);
        const tConstellation = await this.esiUniverseClient.getConstellation(tSystem.constellation_id);
        return { ...transaction, region_id: tConstellation.region_id };
    }
}
