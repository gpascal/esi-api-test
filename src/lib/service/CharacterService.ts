import { AuthDao, CharacterDao, DaoEvent, ICharacter } from '../data';
import { EntityId, EsiCharacterClient, EsiWalletClient } from '../esi';
import { EventHub, EventName } from '../util';

const logger = require('../util/Logger')(module);

export class CharacterService {
    private characterDao: CharacterDao;
    private authDao: AuthDao;
    private esiCharacterClient: EsiCharacterClient;
    private esiWalletClient: EsiWalletClient;

    constructor(eventHub: EventHub, characterDao: CharacterDao, authDao: AuthDao, esiCharacterClient: EsiCharacterClient,
                esiWalletClient: EsiWalletClient) {
        this.characterDao = characterDao;
        this.authDao = authDao;
        this.esiCharacterClient = esiCharacterClient;
        this.esiWalletClient = esiWalletClient;

        authDao.on(DaoEvent.change, () => eventHub.publish(EventName.CHARACTER_UPDATED, null));
    }

    public async updateCharacter(characterId: EntityId): Promise<ICharacter> {
        const characterData = await this.esiCharacterClient.getCharacter(characterId);
        const portraits = await this.esiCharacterClient.getCharacterPortraits(characterId);
        const walletBalance = await this.esiWalletClient.getWalletBalance(characterId);

        const updated = await this.characterDao.update(
            characterId,
            { id: characterId, data: characterData, portraits, walletBalance },
            { upsert: true });
        logger.info(`updated character ${characterData.name}`);
        return updated;
    }

    public async deleteCharacter(characterId: EntityId): Promise<void> {
        await this.authDao.delete(characterId);
        await this.characterDao.delete(characterId);
    }
}
