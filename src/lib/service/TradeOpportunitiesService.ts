import * as _ from 'lodash';
import * as bluebird from 'bluebird';

import { EntityId, EsiMarketClient, EsiMarketOrderType, EsiUniverseClient, IEsiMarketOrderOpen } from '../esi';
import { Duration, EventHub, EventName, IKeyValue, IMultiStageProgressAware, IProgressAware, voidProgress } from '../util';
import { DaoEvent, ITradeOpportunities, ITradeOpportunity, ITypeTradeStats, TradeOpportunitiesDao } from '../data';

const logger = require('../../lib/util/Logger')(module);

type MarketOrdersByType = IKeyValue<IEsiMarketOrderOpen[]>;

export interface ITradeOpportunitiesOptions {
    minBuyPrice?: number;
    maxBuyPrice?: number;
    minTradedVolumePerDay?: number;
    minTradesPerDay?: number;
}

const defaultOptions: ITradeOpportunitiesOptions = {
    minBuyPrice: 10000,
    maxBuyPrice: 100000000,
    minTradedVolumePerDay: 10,
    minTradesPerDay: 3,
};

/**
 * Finds trades between 2 regions for items with acceptable volume and largely different sell prices
 */
export class TradeOpportunitiesService {
    private tradeOpportunitiesDao: TradeOpportunitiesDao;
    private esiMarketClient: EsiMarketClient;
    private esiUniverseClient: EsiUniverseClient;

    constructor(esiMarketClient: EsiMarketClient, esiUniverseClient: EsiUniverseClient, tradeOpportunitiesDao: TradeOpportunitiesDao,
                eventHub: EventHub) {
        this.esiMarketClient = esiMarketClient;
        this.esiUniverseClient = esiUniverseClient;
        this.tradeOpportunitiesDao = tradeOpportunitiesDao;

        tradeOpportunitiesDao.on(DaoEvent.change, (data) => {
            eventHub.publish(EventName.TRADE_OPPORTUNITIES_UPDATED, { data });
        });
    }

    public async computeTradeOpportunities(srcRegionId: EntityId,
                                           dstRegionId: EntityId,
                                           options: ITradeOpportunitiesOptions,
                                           progressFollower: IMultiStageProgressAware = voidProgress): Promise<ITradeOpportunities> {
        const start = _.now();
        const progressState = [
            { stage: 0, progress: 0, label: 'Fetching orders in buy region', total: null, current: 0 },
            { stage: 1, progress: 0, label: 'Fetching orders in sell region', total: null, current: 0 },
            { stage: 2, progress: 0, label: 'Computing opportunities', total: null, current: 0 },
        ];
        progressFollower.progress(progressState);
        const [ getSrcOrdersProgress, getDstOrdersProgress, computeOppsProgress ] = progressState;

        const srcRegion = await this.esiUniverseClient.getRegion(srcRegionId);
        const dstRegion = await this.esiUniverseClient.getRegion(dstRegionId);
        const optionsOverride = _.defaultsDeep(options, defaultOptions);
        logger.info(`Computing new trade opportunities from ${srcRegion.name} to ${dstRegion.name} with`
            + ` options ${JSON.stringify(optionsOverride)}`);

        // FIXME : according to the doc, if no typeId is provided then Buy AND Sell orders are returned
        const getSrcOrdersPromise = this.esiMarketClient.getMarketOrders(srcRegionId, undefined, EsiMarketOrderType.SELL, {
            progress: (current, total) => {
                _.assign(getSrcOrdersProgress, { current, total, progress: total ? current / total : 0 });
                progressFollower.progress(progressState);
            },
        });
        const getDstOrdersPromise = this.esiMarketClient.getMarketOrders(dstRegionId, undefined, EsiMarketOrderType.SELL, {
            progress: (current, total) => {
                _.assign(getDstOrdersProgress, { current, total, progress: total ? current / total : 0 });
                progressFollower.progress(progressState);
            },
        });

        const [ srcOrders, dstOrders ] = await Promise.all([ getSrcOrdersPromise, getDstOrdersPromise ]);

        const srcOrdersByType = _.groupBy(srcOrders, order => order.type_id);
        const dstOrdersByType = _.groupBy(dstOrders, order => order.type_id);

        const bestTrades = await this.findBestTrades(srcRegionId, srcOrdersByType, dstRegionId, dstOrdersByType, optionsOverride, {
            progress: (current, total) => {
                _.assign(computeOppsProgress, { current, total, progress: total ? current / total : 0 });
                progressFollower.progress(progressState);
            },
        });

        const opportunities = {
            id: `${srcRegionId}-${dstRegionId}-${Date.now()}`,
            options,
            date: new Date().toISOString(),
            srcRegionId,
            srcRegionName: srcRegion.name,
            dstRegionId,
            dstRegionName: dstRegion.name,
            opportunities: bestTrades,
        };

        await this.tradeOpportunitiesDao.insert(opportunities);
        logger.info(`Computed ${bestTrades.length} opportunities between ${srcRegion.name} and ${dstRegion.name} in ${_.now() - start}ms`);

        return opportunities;
    }

    private static readonly TRADES_TO_RETURN = 200;

    private async findBestTrades(srcRegionId: EntityId, srcOrders: MarketOrdersByType,
                                 dstRegionId: EntityId, dstOrders: MarketOrdersByType,
                                 options: ITradeOpportunitiesOptions,
                                 progressFollower: IProgressAware): Promise<ITradeOpportunity[]> {
        const inputTypeIds = _.keys(srcOrders);

        progressFollower.progress(0, inputTypeIds.length);
        let processed = 0;

        const trades = await bluebird.map(inputTypeIds, async (typeIdStr: string): Promise<ITradeOpportunity> => {
            try {
                const typeId = parseInt(typeIdStr);
                if (!dstOrders[typeId]) {
                    processed += 1;
                    return null;
                }
                const type = await this.esiUniverseClient.getType(typeId);
                const group = type.group_id ? await this.esiUniverseClient.getGroup(type.group_id) : null;

                const [ srcRegionStats, dstRegionStats ] = await Promise.all([
                    this.computeTypeTradeStatsInRegion(srcRegionId, typeId, srcOrders[typeIdStr]),
                    this.computeTypeTradeStatsInRegion(dstRegionId, typeId, dstOrders[typeIdStr]),
                ]);

                const tradeValue = this.computeTradeValue(srcRegionStats, dstRegionStats, options);

                progressFollower.progress(processed, inputTypeIds.length);
                processed += 1;

                return {
                    tradeValue,
                    typeId,
                    typeName: type.name,
                    groupId: type.group_id,
                    groupName: group && group.name,
                    src: srcRegionStats,
                    dst: dstRegionStats,
                    volume: type.volume,
                };
            } catch (e) {
                processed += 1;
                logger.silly(`Failed to compute trades [type=${typeIdStr}, src=${srcRegionId}, dst=${dstRegionId}] ${e.message}`);
                return null;
            }
        }, { concurrency: 30 });

        progressFollower.progress(inputTypeIds.length, inputTypeIds.length);

        return _<ITradeOpportunity[]>(trades)
            .compact()
            .filter(opp => opp.tradeValue > 1.1)
            .sortBy(['tradeValue'])
            .slice(-1 * TradeOpportunitiesService.TRADES_TO_RETURN)
            .value();
    }

    private computeTradeValue(srcTradeStats: ITypeTradeStats, dstTradeStats: ITypeTradeStats, options: ITradeOpportunitiesOptions): number {
        if (srcTradeStats.minSellPrice < options.minBuyPrice || srcTradeStats.minSellPrice > options.maxBuyPrice) return 0;
        if (dstTradeStats.tradedVolumeByDay < options.minTradedVolumePerDay) return 0;
        if (dstTradeStats.tradesByDay < options.minTradesPerDay) return 0;

        return dstTradeStats.minSellPrice / srcTradeStats.minSellPrice;
    }

    private async computeTypeTradeStatsInRegion(regionId: EntityId,
                                                typeId: EntityId,
                                                typeOrdersInRegion: IEsiMarketOrderOpen[]): Promise<ITypeTradeStats> {
        const cheapestOrder = _.minBy(typeOrdersInRegion, order => order.price);
        const maxSamplePrice = cheapestOrder.price * 1.05;
        const sampleOrders = _.filter(typeOrdersInRegion, order => order.price <= maxSamplePrice);
        const sampleVolume = _.sumBy(sampleOrders, order => order.volume_remain);

        const historyDays = 20;
        const marketHistory = await this.esiMarketClient.getMarketHistory(regionId, typeId);
        const since = Date.now() - (historyDays * Duration.DAY);
        const recentMarketHistory = marketHistory && marketHistory.filter(history => new Date(history.date).getTime() > since);
        const recentTotalVolume = marketHistory && _.sumBy(recentMarketHistory, history => history.volume);
        const recentTotalOrders = marketHistory && _.sumBy(recentMarketHistory, history => history.order_count);
        const recentPriceAveragesByVolume = marketHistory &&
            _.map(recentMarketHistory, history => ({ volume: history.volume, average: history.average }));
        const weightedRecentAveragePrice = marketHistory &&
            _.sumBy(recentPriceAveragesByVolume, p => p.volume * p.average) / _.sumBy(recentPriceAveragesByVolume, 'volume');

        return {
            typeId,
            regionId,
            minSellPrice: cheapestOrder.price,
            availableVolumeAt5Percent: sampleVolume,
            tradedVolumeByDay: recentTotalVolume / historyDays,
            tradesByDay: recentTotalOrders / historyDays,
            averageRecentPrice: weightedRecentAveragePrice,
        };
    }
}
