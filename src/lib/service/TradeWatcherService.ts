import Timeout = NodeJS.Timeout;
import * as _ from 'lodash';
import * as bluebird from 'bluebird';
import * as moment from 'moment';

import {
    DateString,
    EntityId,
    EsiMarketClient,
    EsiMarketOrderType,
    EsiUniverseClient,
    IEsiCharacterMarketOrderOpen,
    IEsiMarketOrderOpen,
    IEsiType,
} from '../esi';
import { Duration, EventHub, EventName } from '../util';

const logger = require('../util/Logger')(module);

export interface ITradeWatchReport {
    type: IEsiType;
    isBestSellOrder: boolean|null;
    bestSellPrice: number;
    sellOrderUpdatableAt: DateString;
    sellOrders: IEsiCharacterMarketOrderOpen[];
}

interface IWatcher {
    characterId: EntityId;
    stationId: EntityId;
    interval: Timeout;
    expiresTimeout: Timeout;
}

export class TradeWatcherService {
    public static get INTERVAL() { return 5 * Duration.MINUTE + 1 * Duration.SECOND; }

    private esiMarketClient: EsiMarketClient;
    private esiUniverseClient: EsiUniverseClient;
    private eventHub: EventHub;
    private liveWatchers: IWatcher[];

    constructor(esiMarketClient: EsiMarketClient, esiUniverseClient: EsiUniverseClient, eventHub: EventHub) {
        this.esiMarketClient = esiMarketClient;
        this.esiUniverseClient = esiUniverseClient;
        this.eventHub = eventHub;
        this.liveWatchers = [];
    }

    public async startTradeWatcher(characterId: EntityId, stationId: EntityId): Promise<void> {
        const watcher = _.find(this.liveWatchers, w => w.characterId === characterId && w.stationId === stationId);
        if (watcher) {
            logger.info(`Reusing existing trade watcher char=${characterId} station=${stationId}`);
            await this.refreshWatcher(watcher.characterId, watcher.stationId);
            return;
        }

        logger.info(`Starting new trade watcher char=${characterId} station=${stationId}`);
        const interval = setInterval(async () => this.refreshWatcher(characterId, stationId), TradeWatcherService.INTERVAL);

        const expiresTimeout = setTimeout(() => {
            logger.info(`Watcher timeout char=${characterId} station=${stationId}`);
            clearInterval(interval);
        }, 1.5 * TradeWatcherService.INTERVAL);
        this.liveWatchers.push({ characterId, stationId, interval, expiresTimeout });

        await this.refreshWatcher(characterId, stationId);
    }

    public bumpWatcher(characterId: EntityId, stationId: EntityId): void {
        const watcher = _.find(this.liveWatchers, w => w.characterId === characterId && w.stationId === stationId);
        if (!watcher) {
            logger.error(`Can not bump watcher char=${characterId} station=${stationId} : not found`);
            return;
        }
        clearTimeout(watcher.expiresTimeout);
        watcher.expiresTimeout = setTimeout(() => {
            logger.info(`Watcher timeout char=${characterId} station=${stationId}`);
            clearInterval(watcher.interval);
            _.remove(this.liveWatchers, w => w.characterId === characterId && w.stationId === stationId);
        }, 3 * TradeWatcherService.INTERVAL);
        logger.debug(`Bumped watcher ${characterId}/${stationId}`);
    }

    private async refreshWatcher(characterId: EntityId, stationId: EntityId): Promise<void> {
        try {
            const nextUpdate = new Date(Date.now() + TradeWatcherService.INTERVAL).toISOString();
            const currentTrades = await this.watchTrades(characterId, stationId);
            this.eventHub.publish(EventName.TRADE_WATCHER_UPDATE, { characterId, stationId, trades: currentTrades, nextUpdate });
        } catch (error) {
            this.eventHub.publish(EventName.TRADE_WATCHER_ERROR, { characterId, stationId, error });
        }
    }

    public async watchTrades(characterId: EntityId, stationId: EntityId): Promise<ITradeWatchReport[]> {
        logger.info(`Watching trades of ${characterId} in ${stationId}`);
        const region = await this.esiUniverseClient.getRegionOfStation(stationId);
        const allCharOrders = await this.esiMarketClient.getCharacterOrders(characterId);
        const allCharOrdersInRegion = _.filter<IEsiCharacterMarketOrderOpen>(allCharOrders, o => o.location_id === stationId);
        const allCharOrdersByType = _.groupBy(allCharOrdersInRegion, o => o.type_id);

        return bluebird.map(_.keys(allCharOrdersByType) as any, async (typeIdStr: string): Promise<ITradeWatchReport> => {
            const typeId = parseInt(typeIdStr);
            const charOrders = allCharOrdersByType[typeIdStr];
            const charSellOrders = _.filter(charOrders, o => !o.is_buy_order);
            const charOrderIds = _.map(charSellOrders, o => o.order_id);
            const type = await this.esiUniverseClient.getType(typeId);

            const marketOrders = await this.esiMarketClient.getMarketOrders(region.region_id, typeId, EsiMarketOrderType.SELL);
            const marketSellOrders = _.filter(marketOrders, o => o.location_id === stationId && !o.is_buy_order);
            const bestSellOffer = _.minBy(marketSellOrders, order => order.price);
            const charMarketOrders = _(charOrderIds)
                .map(id => _.find(marketSellOrders, order => order.order_id === id))
                .compact()
                .value();

            return {
                type,
                bestSellPrice: bestSellOffer && bestSellOffer.price,
                isBestSellOrder: charOrderIds.includes(bestSellOffer.order_id),
                sellOrderUpdatableAt: this.computeUpdatableAt(_.maxBy(charMarketOrders, o => new Date(o.issued))),
                sellOrders: charSellOrders,
            };
        }, { concurrency: 5 });
    }

    private computeUpdatableAt(order: IEsiMarketOrderOpen): DateString {
        if (!order) return null;
        const date = new Date(order.issued);
        return moment(date).add(5, 'minutes').toISOString();
    }
}
