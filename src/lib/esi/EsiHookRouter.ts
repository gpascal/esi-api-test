import { Router, Request, Response } from 'express';
import * as _ from 'lodash';
import * as bluebird from 'bluebird';

const logger = require('../util/Logger')(module);

export interface IHookResponse {
    state: string;
    code: string;
}

export type IHookCallback = (response: IHookResponse) => any|Promise<any>;

export class EsiHookRouter {
    private listeners: IHookCallback[];

    constructor() {
        this.listeners = [];
    }

    public buildRouter(): Router {
        const router: Router = Router({ mergeParams: true });
        router.get('/auth-hook', async (req: Request, res: Response) => {
            try {
                const hookResponse = this.parseHookResponse(req.query);
                logger.info('auth-hook code received: ', hookResponse);
                await this.notifyCodeReceived(hookResponse);

                res.contentType('text/html').send(`
                <html>
                <head><title>Auth OK</title></head>
                <body><div>Auth OK. Closing...</div></body>
                </html>`);
            } catch {
                res.sendStatus(500);
            }
        });
        return router;
    }

    public onCodeReceived(callback: IHookCallback): void {
        this.listeners.push(callback);
    }

    private async notifyCodeReceived(response: IHookResponse): Promise<void> {
        await bluebird.map(this.listeners, listener => listener(response));
    }

    private parseHookResponse(response: unknown): IHookResponse {
        if (typeof response !== 'object') {
            throw new Error(`Can not parse hook response ${JSON.stringify(response)}`);
        }
        const { code, state } = response as any;
        if (!code || !_.isString(code) || !state || !_.isString(state)) {
            throw new Error(`Invalid hook response ${JSON.stringify({ code, state })}`);
        }
        return { code, state };
    }
}
