import { EsiAuthClient } from './EsiAuthClient';
import { Method } from './AbstractEsiClient';
import { AbstractCachedEsiClient } from './AbstractCachedEsiClient';
import {
    EntityId,
    IEsiCategory,
    IEsiConstellation,
    IEsiGroup,
    IEsiRegion,
    IEsiStation,
    IEsiStructure,
    IEsiSystem,
    IEsiType,
    Scope,
} from './types';
import { ICommonDeps, IProgressAware, MultiPageAggregator } from '../util';
import { CacheDao, CacheType } from '../data';

const logger = require('../util/Logger')(module);

export class EsiUniverseClient extends AbstractCachedEsiClient {
    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient, cacheDao: CacheDao) {
        super(commonDeps, esiAuthClient, cacheDao);
        this.logger = logger;
    }

    public async getCategories(): Promise<EntityId[]> {
        return MultiPageAggregator.aggregate<EntityId>(
            page => this.cachedQueryBody(CacheType.CATEGORIES, `/universe/categories`, { query: { page } }));
    }

    public async getCategory(categoryId: EntityId): Promise<IEsiCategory> {
        return this.cachedQueryBody(CacheType.CATEGORY, `/universe/categories/${categoryId}`);
    }

    public async getGroups(progressListener?: IProgressAware): Promise<EntityId[]> {
        return MultiPageAggregator.largeAggregate<EntityId>(
            page => this.query(Method.GET, `/universe/groups`, { query: { page } }),
            page => this.cachedQueryBody(CacheType.GROUPS, `/universe/groups`, { query: { page } }),
            progressListener);
    }

    public async getGroup(groupId: EntityId): Promise<IEsiGroup> {
        return this.cachedQueryBody(CacheType.GROUP, `/universe/groups/${groupId}`);
    }

    public async getConstellations(): Promise<EntityId[]> {
        return this.cachedQueryBody(CacheType.CONSTELLATIONS, `/universe/constellations`);
    }

    public async getConstellation(constellationId: EntityId): Promise<IEsiConstellation> {
        return this.cachedQueryBody(CacheType.CONSTELLATION, `/universe/constellations/${constellationId}`, { forceCache: true });
    }

    public async getRegions(): Promise<EntityId[]> {
        return this.cachedQueryBody(CacheType.REGIONS, `/universe/regions`, { forceCache: true });
    }

    public async getRegion(regionId: EntityId): Promise<IEsiRegion> {
        return this.cachedQueryBody(CacheType.REGION, `/universe/regions/${regionId}`, { forceCache: true });
    }

    public async getStation(stationId: EntityId): Promise<IEsiStation> {
        if (typeof stationId !== 'number') {
            throw new Error('UniverseClient.getStation: parameter must be a number');
        }
        return this.cachedQueryBody(CacheType.STATION, `/universe/stations/${stationId}`, { forceCache: true });
    }

    public async getRegionOfStation(stationId: EntityId): Promise<IEsiRegion> {
        const station = await this.getStation(stationId);
        const system = await this.getSystem(station.system_id);
        const constellation = await this.getConstellation(system.constellation_id);
        return this.getRegion(constellation.region_id);
    }

    public async getStructure(characterId, structureId: EntityId): Promise<IEsiStructure> {
        return this.cachedQueryBody(CacheType.STATION, `/universe/structures/${structureId}`,
            { characterId, scopes: [Scope.UNIVERSE_READ_STRUCTURES], forceCache: true });
    }

    public async getSystems(): Promise<EntityId[]> {
        return this.cachedQueryBody(CacheType.SYSTEMS, `/universe/systems`);
    }

    public async getSystem(systemId: EntityId): Promise<IEsiSystem> {
        return this.cachedQueryBody(CacheType.SYSTEM, `/universe/systems/${systemId}`, { forceCache: true });
    }

    public async getTypes(): Promise<EntityId[]> {
        return MultiPageAggregator.aggregate<EntityId>(
            page => this.queryBody(Method.GET, `/universe/types`, { query: { page } }));
    }

    public async getType(typeId: EntityId): Promise<IEsiType> {
        return this.cachedQueryBody<IEsiType>(CacheType.TYPE, `/universe/types/${typeId}`, { forceCache: true });
    }
}
