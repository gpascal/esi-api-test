import * as _ from 'lodash';
import * as querystring from 'querystring';

import { AbstractEsiClient, Method, QueryParams } from './AbstractEsiClient';
import { EsiAuthClient } from './EsiAuthClient';
import { ICommonDeps } from '../util';
import { CacheDao, CacheType, ICacheEntry } from '../data';

const logger = require('../../lib/util/Logger')(module);

export class AbstractCachedEsiClient extends AbstractEsiClient {
    public preferCache: boolean = false;
    private cacheDao: CacheDao;
    protected forceCache: boolean;

    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient, cacheDao: CacheDao) {
        super(commonDeps, esiAuthClient);
        this.cacheDao = cacheDao;
        this.forceCache = true;
    }

    protected async cachedQueryBody<T extends object>(type: CacheType,
                                                      endpoint: string,
                                                      options: QueryParams = {}): Promise<T> {
        let cacheEntry: ICacheEntry<T>;
        const hash = this.hash(endpoint, options);
        try {
            cacheEntry = await this.cacheDao.get<T>(hash);
            options.debug && logger.debug(`cache match for ${hash}`);
        } catch (err) {
            options.debug && logger.debug(`no cached entry for ${hash}`);
            return this.queryAndCacheResponse<T>(type, hash, null, endpoint, options);
        }

        if (cacheEntry.data && (options.forceCache || this.preferCache)) {
            return cacheEntry.data;
        }

        _.set(options, 'headers.If-None-Match', cacheEntry.etag);
        return this.queryAndCacheResponse<T>(type, hash, cacheEntry, endpoint, options);
    }

    private async queryAndCacheResponse<T extends object>(type: CacheType,
                                           hash: string,
                                           existingCacheEntry: ICacheEntry<T>|null,
                                           endpoint: string,
                                           options: QueryParams = {}): Promise<T> {
        const response = await this.query<T>(Method.GET, endpoint, options);
        if (response.statusCode === 304) {
            return existingCacheEntry.data;
        }
        const etag = _.get(response.headers, 'etag', null);
        if (etag) {
            const cacheEntry: ICacheEntry<T> = {
                etag,
                type,
                id: hash,
                data: response.body,
            };
            await this.cacheDao.update<T>(hash, cacheEntry, { upsert: true });
        } else {
            this.logger.warn(`cached query did not received an etag [${type}] ${hash}`);
        }
        return response.body;
    }

    protected hash(endpoint: string, options: QueryParams): string {
        const queryString = querystring.stringify(options.query);
        return `${endpoint}?${queryString}`;
    }
}
