import { EsiAuthClient } from './EsiAuthClient';
import { AbstractCachedEsiClient } from './AbstractCachedEsiClient';
import { Method } from './AbstractEsiClient';
import { ICommonDeps, MultiPageAggregator } from '../util';
import { CacheDao, CacheType } from '../data';
import { EntityId, Scope, IEsiTransaction, IEsiWalletJournalEntry } from './types';

const logger = require('../util/Logger')(module);

export class EsiWalletClient extends AbstractCachedEsiClient {
    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient, cacheDao: CacheDao) {
        super(commonDeps, esiAuthClient, cacheDao);
        this.logger = logger;
    }

    public async getWalletBalance(characterId: EntityId): Promise<number> {
        return this.queryBody(Method.GET, `/characters/${characterId}/wallet/`,
            { characterId, scopes: [Scope.WALLET_READ_CHARACTER_WALLET], debug: true }) as any;
    }

    public async getCharacterJournal(characterId: EntityId, fromId: EntityId = undefined): Promise<IEsiWalletJournalEntry[]> {
        return MultiPageAggregator.largeAggregate<IEsiWalletJournalEntry>(
            page => this.query(Method.GET, `/characters/${characterId}/wallet/journal`, { query: { page } }),
            page => this.cachedQueryBody(CacheType.WALLET_JOURNAL, `/characters/${characterId}/wallet/journal/`,
                { query: { from_id: fromId, page }, characterId, scopes: [Scope.WALLET_READ_CHARACTER_WALLET] }));
    }

    public async getCharacterTransactions(characterId: EntityId, fromId: EntityId = undefined): Promise<IEsiTransaction[]> {
        return this.cachedQueryBody(CacheType.WALLET_TRANSACTION, `/characters/${characterId}/wallet/transactions/`,
            { query: { from_id: fromId }, characterId, scopes: [Scope.WALLET_READ_CHARACTER_WALLET] });
    }
}
