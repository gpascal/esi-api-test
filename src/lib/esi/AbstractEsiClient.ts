import * as _ from 'lodash';
import * as got from 'got';
import { Response } from 'got'; // tslint:disable-line
import * as url from 'url';
import { Logger } from 'winston';
import * as querystring from 'querystring';

import { IConfig } from '../config';
import { EsiAuthClient } from './EsiAuthClient';
import { ICommonDeps } from '../util';
import { EntityId, Scope } from './types';

const logger = require('../util/Logger')(module);

declare type GotJSONOptions = got.GotJSONOptions;

export enum Method {
    GET = 'GET',
    POST = 'POST',
    OPTIONS = 'OPTIONS',
}

export type QueryParams = Partial<GotJSONOptions & {
    characterId: EntityId;
    debug: boolean;
    forceCache: boolean;
    scopes: Scope[];
    silent: boolean;
}>;

export abstract class AbstractEsiClient {
    protected logger: Logger; // Should be overrided in each subclass to get the correct log prefix
    protected config: IConfig;
    protected esiAuthClient: EsiAuthClient;

    protected constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient) {
        this.logger = logger;
        this.config = commonDeps.config;
        this.esiAuthClient = esiAuthClient;
    }

    protected async queryBody<T extends object>(method: Method, endpoint: string, options: QueryParams = {}): Promise<T> {
        return (await this.query<T>(method, endpoint, options)).body;
    }

    protected async query<T extends object>(method: Method, endpoint: string, options: QueryParams = {}): Promise<Response<T>> {
        const { apiUrl } = this.config.esi;
        const token = options.characterId ? await this.esiAuthClient.getAccessToken(options.characterId, options.scopes) : '';
        const uri = `${apiUrl}/${endpoint}`.replace(/(?<!:)\/{2,}/g, '/');

        const defaultRequestParams: got.GotJSONOptions = {
            method,
            json: true,
            headers: {
                Authorization: `Bearer ${token}`,
                Host: url.parse(apiUrl).hostname,
            },
        };
        const requestParams = _.defaultsDeep(defaultRequestParams, options);
        const queryString = _.isEmpty(requestParams.query) ? '' : `?${querystring.stringify(requestParams.query)}`;

        try {
            const response = await got(uri, requestParams);
            options.debug && this.logger.info(`Outgoing ${method} ${uri}${queryString} : ${response.statusCode} ${response.statusMessage}`);
            return response;
        } catch (err) {
            const httpErr = err as got.GotError;
            !options.silent && this.logger.error(`Outgoing ${httpErr.method} ${httpErr.url}${queryString} token=${token.slice(0, 25)}`);
            throw err;
        }
    }
}
