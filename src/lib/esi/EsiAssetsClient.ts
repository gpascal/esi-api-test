import { EsiAuthClient } from './EsiAuthClient';
import { AbstractCachedEsiClient } from './AbstractCachedEsiClient';
import { ICommonDeps, MultiPageAggregator } from '../util';
import { EntityId, IEsiCharacterAsset } from './types';
import { CacheDao, CacheType } from '../data';

const logger = require('../util/Logger')(module);

export class EsiAssetsClient extends AbstractCachedEsiClient {
    constructor(commonDeps: ICommonDeps, esiAuthClient: EsiAuthClient, cacheDao: CacheDao) {
        super(commonDeps, esiAuthClient, cacheDao);
        this.logger = logger;
    }

    public async getCharacterAssets(characterId: EntityId): Promise<IEsiCharacterAsset[]> {
        return MultiPageAggregator.aggregate<IEsiCharacterAsset>(
            page => this.cachedQueryBody(CacheType.CHARACTER_ASSETS,
                `/characters/${characterId}/assets`, { characterId, query: { page } }));
    }
}
