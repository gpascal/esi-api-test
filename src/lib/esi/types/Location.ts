import { EntityId } from './index';

export interface IEsiLocation {
    system_id: EntityId;
    station_id?: EntityId;
    structure_id?: EntityId;
}
