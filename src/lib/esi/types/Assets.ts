import { DateString, EntityId } from '.';
import { isBoolean } from 'util';

export enum LocationType {
    STATION = 'station',
    SOLAR_SYSTEM = 'solar_system',
    OTHER = 'other',
}

export interface IEsiCharacterAsset {
    is_blueprint_copy: boolean;
    is_singleton: boolean;
    item_id: EntityId;
    location_flag: string;
    location_id: EntityId;
    location_type: LocationType;
    quantity: number;
    type_id: EntityId;
}
