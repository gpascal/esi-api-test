import { DateString } from '.';

export interface IEsiStatus {
    players: number;
    server_version: string;
    start_time: DateString;
    vip: boolean;
}
