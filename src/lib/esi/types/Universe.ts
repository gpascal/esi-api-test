import { EntityId } from './index';

export interface IEsiPosition {
    x: number;
    y: number;
    z: number;
}

export interface IEsiRegion {
    region_id: EntityId;
    name: string;
    description?: string;
    constellations: EntityId[];
}

export interface IEsiConstellation {
    constellation_id: EntityId;
    name: string;
    position: IEsiPosition;
    region_id: EntityId;
    systems: EntityId[];
}

export interface IEsiSystem {
    constellation_id: EntityId;
    star_id: EntityId;
    system_id: EntityId;
    name: string;
    security_status: number;
    security_class: string;
    position: IEsiPosition;
    planets: EntityId[];
    stargates: EntityId[];
    stations: EntityId[];
}

export interface IEsiStation {
    station_id: EntityId;
    system_id: EntityId;
    type_id: EntityId;
    name: string;
    position: IEsiPosition;
    max_dockable_ship_volume: number;
    reprocessing_efficiency: number;
    reprocessing_stations_take: number;
    services: IEsiService[];
    race_id: EntityId;
}

export enum IEsiService {
    BOUNTY_MISSIONS = 'bounty-missions',
    ASSASINATION_MISSIONS = 'assasination-missions',
    COURIER_MISSIONS = 'courier-missions',
    INTERBUS = 'interbus',
    REPROCESSING_PLANT = 'reprocessing-plant',
    REFINERY = 'refinery',
    MARKET = 'market',
    BLACK_MARKET = 'black-market',
    STOCK_EXCHANGE = 'stock-exchange',
    CLONING = 'cloning',
    SURGERY = 'surgery',
    DNA_THERAPY = 'dna-therapy',
    REPAIR_FACILITIES = 'repair-facilities',
    FACTORY = 'factory',
    LABRATORY = 'labratory',
    GAMBLING = 'gambling',
    FITTING = 'fitting',
    PAINTSHOP = 'paintshop',
    NEWS = 'news',
    STORAGE = 'storage',
    INSURANCE = 'insurance',
    DOCKING = 'docking',
    OFFICE_RENTAL = 'office-rental',
    JUMP_CLONE_FACILITY = 'jump-clone-facility',
    LOYALTY_POINT_STORE = 'loyalty-point-store',
    NAVY_OFFICES = 'navy-offices',
    SECURITY_OFFICES = 'security-offices',
}

export interface IEsiStructure {
    owner_id: EntityId;
    solar_system_id: EntityId;
    name: string;
    position?: IEsiPosition;
    type_id?: EntityId;
}

export interface IEsiGraphic {
    graphic_id: EntityId;
    collision_file: string;
    graphic_file: string;
    icon_folder: string;
    sof_dna: string;
    sof_fation_name: string;
    sof_hull_name: string;
    sof_race_name: string;
}

export interface IEsiType {
    group_id: EntityId;
    type_id: EntityId;
    name: string;
    description: string;
    market_group_id: EntityId;
    mass: number;
    packaged_volume: number;
    portion_size: number;
    radius: number;
    capacity: number;
    volume: number;
    dogma_attributes: { attribute_id: EntityId, value: number }[];
    dogma_effects: { effect_id: EntityId, is_default: boolean }[];
    graphic_id: EntityId;
    icon_id: EntityId;
    published: boolean;
}

export interface IEsiGroup {
    category_id: EntityId;
    group_id: EntityId;
    name: string;
    published: boolean;
    types: EntityId[];
}

export interface IEsiCategory {
    category_id: EntityId;
    groups: EntityId[];
    name: string;
    published: boolean;
}
