import { DateString, EntityId } from '.';

export interface IEsiCharacterPublicData {
    ancestry_id: EntityId;
    birthday: DateString;
    bloodline_id: EntityId;
    corporation_id: EntityId;
    description: string;
    gender: string;
    name: string;
    race_id: EntityId;
    security_status: number;
}

export interface IEsiCharacterPortrait {
    px64x64: string;
    px128x128: string;
    px256x256: string;
    px512x512: string;
}

export enum EsiCharacterStandingType {
    AGENT = 'agent',
    NPC_CORP = 'npc_corp',
    FACTION = 'faction',
}

export interface IEsiCharacterStanding {
    from_id: EntityId;
    from_type: EsiCharacterStandingType;
    standing: number;
}
