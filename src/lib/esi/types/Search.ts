import { EntityId } from '.';

export interface IEsiSearchResult {
    agent: EntityId[];
    alliance: EntityId[];
    character: EntityId[];
    constellation: EntityId[];
    corporation: EntityId[];
    faction: EntityId[];
    inventory_type: EntityId[];
    region: EntityId[];
    solar_system: EntityId[];
    station: EntityId[];
    structure: EntityId[];
}
