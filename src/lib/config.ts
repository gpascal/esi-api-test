import { Scope } from './esi/types';
import _ = require('lodash');
import * as fs from 'fs';

export interface IConfig {
    esi: {
        apiUrl: string;
        auth: {
            authTimeout: number;
            oauthEndpoint: string;
            hookServerPort: number;
            clientId: string;
            secretKey: string;
            authCodeTtl: number;
        };
        scopes: Scope[];
    };
    http: {
        headless: boolean;
        port: number;
    };
    mongo: {
        host: string;
        port: number;
        db: string;
    };
    cache: {
        path: string;
    };
    test: {
        ids: {
            eloKhamez: number;
            calaen: number;
        },
        corporationId: number;
    };
}

const defaultConfig: IConfig = {
    esi: {
        apiUrl: 'https://esi.evetech.net/latest',
        auth: {
            authTimeout: 20000,
            oauthEndpoint: 'https://login.eveonline.com/v2/oauth',
            authCodeTtl: (5 * 60 * 1000),
            hookServerPort: 3002,
            clientId: 'aaeb63116b6d48dd86fa956619ab0d23',
            secretKey: 'RZjJSGzcZCL7Ic1lR5nTTrIaBkmlZVJrq0bagzEQ',
        },
        scopes: [
            Scope.UNIVERSE_READ_STRUCTURES,
            Scope.WALLET_READ_CHARACTER_WALLET,
            Scope.MARKETS_READ_CHARACTER_ORDERS,
            Scope.CHARACTERS_READ_STANDINGS,
            Scope.CHARACTERSTATS_READ,
            Scope.PUBLIC_DATA,
            Scope.ASSETS_READ_ASSETS,
            Scope.CONTRACTS_READ_CHARACTER_CONTRACTS,
            Scope.MARKETS_STRUCTURE_MARKETS,
            Scope.SEARCH_SEARCH_STRUCTURES,
        ],
    },
    http: {
        headless: false,
        port: 3009,
    },
    mongo: {
        host: 'localhost',
        port: 27017,
        db: 'esi-test',
    },
    cache: {
        path: 'D:/dev/data',
    },
    test: {
        ids: {
            calaen: 95725892,
            eloKhamez: 2114776675,
        },
        corporationId: 0,
    },
};

const confOverridePath = process.env.CONF_FILE;
const userConf = confOverridePath ? JSON.parse(fs.readFileSync(confOverridePath, { encoding: 'utf8' })) : {};

export const config: IConfig = _.defaultsDeep(userConf, defaultConfig);

(function validateConfig(c) {
    if (c.esi.auth.oauthEndpoint.endsWith('/')) {
        throw new Error('invalid oauth endpoint');
    }
})(config);
