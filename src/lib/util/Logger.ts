import Module = NodeJS.Module;
import * as _ from 'lodash';
import { transports, format, createLogger } from 'winston';
import * as path from 'path';
import chalk from 'chalk';

const { combine, timestamp, label, printf } = format;

export = (module: Module) => {
    const filename = path.basename(module.id, path.extname(module.id));
    return createLogger({
        level: 'debug',
        format: combine(
            timestamp({ format: 'HH:mm:ss.SSS' }),
            label({ label: filename }),
            printf(info => `${info.timestamp} ${color(info.level)} ${pad(`[${info.label}]`, 30)} ${info.message}`),
        ),
        transports: [new transports.Console()],
    });
};

function pad(str, length) {
    return _.padEnd(str, length);
}
function color(level) {
    const levelToColor = {
        silly: 'gray',
        info: 'green',
        debug: 'blue',
        warn: 'yellow',
        error: 'red',
    };
    return chalk[levelToColor[level]](pad(level, 6));
}
