import { IConfig } from '../config';
import {
    CharactersRouter,
    DataRouter,
    EventsRouter,
    GraphRouter,
    HttpServer,
    TradeOpportunitiesRouter,
    TradeReportsRouter,
    TradesWatcherRouter,
} from '../api';
import {
    AuthDao,
    CacheDao,
    CharacterDao,
    GraphDao,
    TradeOpportunitiesDao,
    TradeReportDao,
} from '../data';
import {
    EsiAssetsClient,
    EsiAuthClient,
    EsiCharacterClient,
    EsiHookRouter,
    EsiMarketClient,
    EsiUniverseClient,
    EsiWalletClient,
} from '../esi';
import {
    TradeOpportunitiesService,
    TradeWatcherService,
    TradeReporterService,
    EsiDataService,
    CharacterService, LiveGraphService, GraphService,
} from '../service';
import { EventHub } from './EventHub';

export interface ICommonDeps {
    config: IConfig;
    eventHub: EventHub;
}

export interface IEsiClients {
    esiAssetsClient: EsiAssetsClient;
    esiAuthClient: EsiAuthClient;
    esiCharacterClient: EsiCharacterClient;
    esiMarketClient: EsiMarketClient;
    esiUniverseClient: EsiUniverseClient;
    esiWalletClient: EsiWalletClient;
}

export interface IDao {
    authDao: AuthDao;
    cacheDao: CacheDao;
    characterDao: CharacterDao;
    graphDao: GraphDao;
    tradeReportDao: TradeReportDao;
    tradeOpportunitiesDao: TradeOpportunitiesDao;
}

export interface IServices {
    tradeOpportunitiesService: TradeOpportunitiesService;
    tradeWatcherService: TradeWatcherService;
    tradeReporterService: TradeReporterService;
    esiDataService: EsiDataService;
    characterService: CharacterService;
    liveGraphService: LiveGraphService;
    graphService: GraphService;
}

export interface IHttp {
    server: HttpServer;
}

export interface IDependencies {
    common: ICommonDeps;
    esiClients: IEsiClients;
    dao: IDao;
    services: IServices;
    http: IHttp;
}

export class DependenciesBuilder {
    public build(config: IConfig): IDependencies {
        const common = this.buildCommon(config);
        const dao = this.buildDaos(common);
        const esiClients = this.buildEsiClients(common, dao);
        const services = this.buildServices(common, dao, esiClients);
        const http = this.buildHttp(common, dao, esiClients, services);

        return {
            common,
            esiClients,
            dao,
            services,
            http,
        };
    }

    private buildCommon(config: IConfig): ICommonDeps {
        return {
            config,
            eventHub: new EventHub(),
        };
    }

    private buildDaos(common: ICommonDeps): IDao {
        const authDao = new AuthDao(common);
        const characterDao = new CharacterDao(common);
        const cacheDao = new CacheDao(common);
        const graphDao = new GraphDao(common);
        const tradeReportDao = new TradeReportDao(common);
        const tradeOpportunitiesDao = new TradeOpportunitiesDao(common);

        return {
            authDao,
            characterDao,
            cacheDao,
            graphDao,
            tradeReportDao,
            tradeOpportunitiesDao,
        };
    }

    private buildEsiClients(common: ICommonDeps, dao: IDao): IEsiClients {
        const esiHookRouter = new EsiHookRouter();
        const esiAuthClient = new EsiAuthClient(common, esiHookRouter, dao.authDao);
        const esiCharacterClient = new EsiCharacterClient(common, esiAuthClient);
        const esiMarketClient = new EsiMarketClient(common, esiAuthClient, dao.cacheDao);
        const esiUniverseClient = new EsiUniverseClient(common, esiAuthClient, dao.cacheDao);
        const esiWalletClient = new EsiWalletClient(common, esiAuthClient, dao.cacheDao);
        const esiAssetsClient = new EsiAssetsClient(common, esiAuthClient, dao.cacheDao);

        return {
            esiAssetsClient,
            esiAuthClient,
            esiCharacterClient,
            esiMarketClient,
            esiUniverseClient,
            esiWalletClient,
        };
    }

    private buildServices(common: ICommonDeps, dao: IDao, esi: IEsiClients): IServices {
        const tradeOpportunitiesService = new TradeOpportunitiesService(
            esi.esiMarketClient,
            esi.esiUniverseClient,
            dao.tradeOpportunitiesDao,
            common.eventHub);
        const tradeWatcherService = new TradeWatcherService(esi.esiMarketClient, esi.esiUniverseClient, common.eventHub);
        const tradeReporterService = new TradeReporterService(
            dao.tradeReportDao,
            esi.esiMarketClient,
            esi.esiUniverseClient,
            esi.esiWalletClient,
            common.eventHub);
        const esiDataService = new EsiDataService(esi.esiUniverseClient);
        const characterService = new CharacterService(
            common.eventHub,
            dao.characterDao,
            dao.authDao,
            esi.esiCharacterClient,
            esi.esiWalletClient);
        const liveGraphService = new LiveGraphService(
            dao.graphDao,
            esi.esiWalletClient,
            esi.esiMarketClient,
            esi.esiAssetsClient,
            esi.esiUniverseClient);
        const graphService = new GraphService(
            dao.graphDao,
            esi.esiWalletClient,
            esi.esiMarketClient,
            esi.esiAssetsClient,
            esi.esiUniverseClient);

        return {
            tradeOpportunitiesService,
            tradeWatcherService,
            tradeReporterService,
            esiDataService,
            characterService,
            liveGraphService,
            graphService,
        };
    }

    private buildHttp(common: ICommonDeps, dao: IDao, esiClients: IEsiClients, services: IServices): IHttp {
        const characterRouter = new CharactersRouter(
            common.config,
            common.eventHub,
            dao.characterDao,
            services.characterService,
            esiClients.esiAuthClient);
        const dataRouter = new DataRouter(
            dao.characterDao,
            services.esiDataService);
        const tradeReportsRouter = new TradeReportsRouter(
            common.config,
            common.eventHub,
            services.tradeReporterService,
            dao.tradeReportDao);
        const tradeOpportunitiesRouter = new TradeOpportunitiesRouter(
            common.config,
            common.eventHub,
            services.tradeOpportunitiesService,
            dao.tradeOpportunitiesDao);
        const tradeWatcherRouter = new TradesWatcherRouter(
            common.config,
            common.eventHub,
            services.tradeWatcherService);
        const eventRouter = new EventsRouter(common.eventHub);
        const graphRouter = new GraphRouter(dao.graphDao, services.graphService);

        return {
            server: new HttpServer(common, [
                characterRouter,
                dataRouter,
                tradeReportsRouter,
                tradeOpportunitiesRouter,
                tradeWatcherRouter,
                eventRouter,
                graphRouter,
            ]),
        };
    }
}
