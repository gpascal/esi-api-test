import { Response } from 'got';
import * as bluebird from 'bluebird';
import * as _ from 'lodash';
import { IProgressAware, voidProgress } from './Types';

export class MultiPageAggregator {
    public static async aggregate<T>(getPage: (pageIndex: number) => Promise<T[]>): Promise<T[]> {
        const aggregated: T[] = [];

        let currentPage = [];
        let pageIndex = 1;
        do {
            currentPage = await getPage(pageIndex);
            aggregated.push(...currentPage);
            pageIndex += 1;
        } while (currentPage.length > 0);

        return aggregated;
    }

    public static async largeAggregate<T>(getPage: (pageIndex: number) => Promise<Response<T[]>>,
                                          cachedGetBody: (pageIndex: number) => Promise<T[]>,
                                          progressListener: IProgressAware = voidProgress): Promise<T[]> {
        progressListener.progress(0, null);
        const firstPage = await getPage(1);
        const totalPage = parseInt(firstPage.headers['x-pages'] as string);
        progressListener.progress(0, totalPage);
        let processed = 0;

        const pages = await bluebird.map(_.range(1, totalPage + 1), async (page: number) => {
            const pageBody = await cachedGetBody(page);
            processed += 1;
            progressListener.progress(processed, totalPage);
            return pageBody;
        }, { concurrency: 10 });

        progressListener.progress(totalPage, totalPage);
        return _.flatten(pages);
    }
}
