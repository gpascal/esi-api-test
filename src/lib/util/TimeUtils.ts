
export enum Duration {
    DAY = 24 * 60 * 60 * 1000,
    HOUR = 60 * 60 * 1000,
    MINUTE = 60 * 1000,
    SECOND = 1000,
}
