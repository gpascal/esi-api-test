export enum EventName {
    CHARACTER_UPDATED = 'character-updated',
    CHARACTER_AUTH_ERROR = 'character-auth-error',

    TRADE_REPORTS_COMPUTED = 'trade-reports-computed',
    TRADE_REPORTS_ERROR = 'trade-reports-error',
    TRADE_REPORTS_PROGRESS = 'trade-reports-progress',
    TRADE_REPORTS_UPDATED = 'trade-reports-updated',

    TRADE_WATCHER_ERROR = 'trade-watcher-error',
    TRADE_WATCHER_PROGRESS = 'trade-watcher-progress',
    TRADE_WATCHER_UPDATE = 'trade-watcher-update',

    TRADE_OPPORTUNITIES_COMPUTED = 'trade-opportunities-computed',
    TRADE_OPPORTUNITIES_ERROR = 'trade-opportunities-error',
    TRADE_OPPORTUNITIES_PROGRESS = 'trade-opportunities-progress',
    TRADE_OPPORTUNITIES_UPDATED = 'trade-opportunities-updated',
}

export interface IEventHubListener {
    publish: (eventName: EventName, eventData: any) => void;
}

export class EventHub {
    private listener: IEventHubListener;

    constructor() {}

    public setListener(value: IEventHubListener) {
        this.listener = value;
    }

    public publish(eventName: EventName, eventData: any) {
        this.listener.publish(eventName, JSON.stringify(eventData));
    }
}
