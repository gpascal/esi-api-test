module.exports = {
    apps: [{
        name: 'esi-app',
        script: 'build/main.js',
        instances: 1,
        autorestart: true,
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production',
            CONF_FILE: '/home/esi-user/config/esi-config.json',
        },
    }],

    deploy: {
        production: {
            user: 'esi-user',
            host: '185.212.225.90',
            ref: 'origin/master',
            repo: 'git@gitlab.com:gpascal/esi-api-test.git',
            path: '/home/esi-user/lib/',
            'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
        },
    },
};
